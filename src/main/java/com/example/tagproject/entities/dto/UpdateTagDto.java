package com.example.tagproject.entities.dto;

import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UpdateTagDto {

    Optional<String> content;
    Optional<String> description;
    Optional<String> title;

    public UpdateTagDto(Optional<String> content,
                        Optional<String> description,
                        Optional<String> title) {
        this.content = content;
        this.description = description;
        this.title = title;
    }

    public Optional<String> getContent() {
        return content;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public Optional<String> getTitle() {
        return title;
    }
}
