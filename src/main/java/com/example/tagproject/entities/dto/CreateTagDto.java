package com.example.tagproject.entities.dto;

import org.springframework.stereotype.Component;

import javax.validation.constraints.Size;

@Component
public class CreateTagDto {

    @Size(min = 5)
    String content;

    @Size(min = 5, max = 100, message = "Description should be between 5 and 100 characters")
    String description;

    @Size(min = 3, max = 50, message = "Title should be between 3 and 50 characters.")
    String title;

    public CreateTagDto() {

    }

    public String getContent() {
        return content;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }
}
