package com.example.tagproject.entities.dto;


import org.springframework.stereotype.Component;

import javax.validation.constraints.*;

@Component
public class CreateUserDto {

    @NotEmpty(message = "Please enter email address")
    @Size(max = 50)
    @Email
    String email;

    @NotEmpty(message = "Username must not be empty!")
    @Size(min = 5, max = 30, message = "Username must be minimum 5 and maximum 30 characters!")
    String username;

    @NotEmpty(message = "Password should not be empty!")
    String password;


    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
