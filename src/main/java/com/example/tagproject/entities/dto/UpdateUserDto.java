package com.example.tagproject.entities.dto;

import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UpdateUserDto {

    Optional<String> email;
    Optional<String> password;
    Optional<String> username;

    public UpdateUserDto(Optional<String> username,
                         Optional<String> password,
                         Optional<String> email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<String> getPassword() {
        return password;
    }

    public Optional<String> getEmail() {
        return email;
    }
}
