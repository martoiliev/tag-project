package com.example.tagproject.controllers;

import com.example.tagproject.entities.Tag;
import com.example.tagproject.entities.User;
import com.example.tagproject.entities.dto.CreateTagDto;
import com.example.tagproject.entities.dto.UpdateTagDto;
import com.example.tagproject.exceptions.EntityNotFoundException;
import com.example.tagproject.exceptions.UnauthorizedOperationException;
import com.example.tagproject.services.TagService;
import com.example.tagproject.services.mappers.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagController {

    private final TagService tagService;
    private final TagMapper tagMapper;
    private final AuthenticationHandler authenticationHandler;

    @Autowired
    public TagController(TagService tagService, TagMapper tagMapper, AuthenticationHandler authenticationHandler) {
        this.tagService = tagService;
        this.tagMapper = tagMapper;
        this.authenticationHandler = authenticationHandler;
    }

    @GetMapping
    public List<Tag> getAll() {
        return tagService.getAll();
    }

    @GetMapping("/{id}")
    public Tag getTagById(@PathVariable int id) {
        return tagService.getTagById(id);
    }

    @PostMapping
    public Tag createTag(@RequestHeader HttpHeaders headers, @Valid @RequestBody CreateTagDto createTagDto) {
        try {
            User user = authenticationHandler.getUserByUsername(headers);
            Tag tag = tagMapper.dtoToObject(createTagDto, user);

            return tagService.createTag(tag);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Tag deleteTag(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHandler.getUserByUsername(headers);

            return tagService.deleteTagById(user.getId(), id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public Tag updateTag(@RequestHeader HttpHeaders headers,
                         @PathVariable int id,
                         @Valid @RequestBody UpdateTagDto updateTagDto) {
        try {
            User user = authenticationHandler.getUserByUsername(headers);
            Tag tag = tagService.getTagById(id);

            return tagService.updateTag(tag, updateTagDto, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}
