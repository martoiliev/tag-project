package com.example.tagproject.controllers;

import com.example.tagproject.entities.User;
import com.example.tagproject.exceptions.EntityNotFoundException;
import com.example.tagproject.exceptions.UnauthorizedOperationException;
import com.example.tagproject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;


@Component
public class AuthenticationHandler {

    private static final String AUTHORIZATION = "Authorization";
    private final UserService userService;

    @Autowired
    public AuthenticationHandler(UserService userService) {
        this.userService = userService;
    }

    public User getUserByUsername(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Requested resource request login.");
        }

        try {
            String username = headers.getFirst(AUTHORIZATION);
            return userService.getUserByUsername(username);

        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("Wrong username!");
        }
    }
}
