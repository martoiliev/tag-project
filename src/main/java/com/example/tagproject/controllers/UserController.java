package com.example.tagproject.controllers;

import com.example.tagproject.entities.User;
import com.example.tagproject.entities.dto.CreateUserDto;
import com.example.tagproject.entities.dto.UpdateUserDto;
import com.example.tagproject.exceptions.UnauthorizedOperationException;
import com.example.tagproject.services.UserService;
import com.example.tagproject.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHandler authenticationHandler;

    @Autowired
    public UserController(UserService userService, UserMapper userMapper, AuthenticationHandler authenticationHandler) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHandler = authenticationHandler;
    }

    @GetMapping
    public List<User> getAllUsers(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHandler.getUserByUsername(headers);
            return userService.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getUserById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHandler.getUserByUsername(headers);
            return userService.getUserById(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public User createUser(@Valid @RequestBody CreateUserDto createUserDto) {
        try {
            return userService.createUser(userMapper.dtoToObject(createUserDto));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public User deleteUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHandler.getUserByUsername(headers);
            return userService.deleteUserById(user,id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalAccessException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public User updateUser(@RequestHeader HttpHeaders headers,
                           @PathVariable int id,
                           @RequestBody UpdateUserDto updateUserDto) {
        try {
            User user = authenticationHandler.getUserByUsername(headers);

            return userService.updateUser(user, id, updateUserDto);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalAccessException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
