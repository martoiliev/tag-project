package com.example.tagproject.services;

import com.example.tagproject.entities.Tag;
import com.example.tagproject.entities.User;
import com.example.tagproject.entities.dto.UpdateTagDto;
import com.example.tagproject.exceptions.EntityNotFoundException;
import com.example.tagproject.exceptions.UnauthorizedOperationException;
import com.example.tagproject.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@PropertySource("classpath:application.properties")
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public Tag getTagById(int id) {
        try {
            return tagRepository.getTagById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public Tag getTagByTitle(String title) {
        try {
            return tagRepository.getTagByTitle(title);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public Tag createTag(Tag tag) {
        try {
            tagRepository.getTagByTitle(tag.getTitle());
        } catch (EntityNotFoundException e) {
            return tagRepository.createTag(tag);
        }

        throw new IllegalArgumentException("Please enter different title.");
    }

    @Override
    public Tag updateTag(Tag tagToUpdate, UpdateTagDto updateTagDto, User user) {
        try {
            checkForValidOwner(tagToUpdate.getUser().getId(), user.getId());

            if (updateTagDto.getContent().isPresent()) {
                tagToUpdate.setContent(updateTagDto.getContent().get());
            }

            if (updateTagDto.getDescription().isPresent()) {
                tagToUpdate.setDescription(updateTagDto.getDescription().get());
            }

            if (updateTagDto.getTitle().isPresent()) {
                tagToUpdate.setTitle(updateTagDto.getTitle().get());
            }

            return tagRepository.updateTag(tagToUpdate);

        } catch (EntityNotFoundException e) {
          throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    @Override
    public Tag deleteTagById(int userId, int id) {
        try {
            Tag tag = tagRepository.getTagById(id);
            if (userId != tag.getUser().getId()) {
                throw new UnauthorizedOperationException("Only owner of the tag can delete it.");
            }

            return tagRepository.deleteTag(tag);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private void checkForValidOwner(int tagUserId, int userId) {
        if (tagUserId != userId) {
            throw new UnauthorizedOperationException("You must be the owner of the tag in order for you to update it.");
        }
    }

}
