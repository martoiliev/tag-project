package com.example.tagproject.services;

import com.example.tagproject.entities.Tag;
import com.example.tagproject.entities.User;
import com.example.tagproject.entities.dto.UpdateTagDto;

import java.util.List;

public interface TagService {

    List<Tag> getAll();

    Tag getTagById(int id);

    Tag getTagByTitle(String title);

    Tag createTag(Tag tag);

    Tag updateTag(Tag tagToUpdate, UpdateTagDto updateTagDto, User user);

    Tag deleteTagById(int userId, int id);
}
