package com.example.tagproject.services;

import com.example.tagproject.entities.User;
import com.example.tagproject.entities.dto.UpdateUserDto;

import java.util.*;

public interface UserService {

    List<User> getAll();

    User getUserById(int id);

    User getUserByUsername(String username);

    User createUser(User user);

    User updateUser(User user ,int id, UpdateUserDto updateUserDto) throws IllegalAccessException;

    User deleteUserById(User user, int id) throws IllegalAccessException;


}
