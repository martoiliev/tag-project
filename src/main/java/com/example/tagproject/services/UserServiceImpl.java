package com.example.tagproject.services;

import com.example.tagproject.entities.User;
import com.example.tagproject.entities.dto.UpdateUserDto;
import com.example.tagproject.exceptions.EntityNotFoundException;
import com.example.tagproject.exceptions.UnauthorizedOperationException;
import com.example.tagproject.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@PropertySource("classpath:application.properties")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getUserById(int id) {
        return userRepository.getUserById(id);

    }

    @Override
    public User getUserByUsername(String username) {
        try {
            return userRepository.getUserByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public User createUser(User user) {
        try {
            userRepository.getUserByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            try {
                userRepository.getUserByUsername(user.getUsername());
            } catch (EntityNotFoundException ex) {
                return userRepository.createUser(user);
            }
        }

        throw new IllegalArgumentException("Please sign up using different email/username.");
    }

    @Override
    public User updateUser(User user, int id, UpdateUserDto updateUserDto) throws IllegalAccessException {
        try {
            User userToUpdate = getUserById(id);
            checkForUserIds(user.getId(), id);

            if (updateUserDto.getEmail().isPresent()) {
                validateEmail(updateUserDto.getEmail().get());
                userToUpdate.setEmail(updateUserDto.getEmail().get());
            }

            if (updateUserDto.getPassword().isPresent()) {
                userToUpdate.setPasswordHash("__" + updateUserDto.getPassword().get() + "hash3264");
            }

            if (updateUserDto.getUsername().isPresent()) {
                userToUpdate.setUsername(updateUserDto.getUsername().get());
            }

            return userRepository.updateUser(userToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new IllegalAccessException("You must be the logged user.");
        }
    }

    @Override
    public User deleteUserById(User user, int id) throws IllegalAccessException {
        try {
            User userToDelete = userRepository.getUserById(id);
            checkForUserIds(user.getId(), id);

            return userRepository.deleteUser(userToDelete);
        } catch (EntityNotFoundException e) {
            throw new IllegalArgumentException("User with id:" + id + " does not exists.");
        } catch (UnauthorizedOperationException e) {
            throw new IllegalAccessException("Only user owner can delete his account.");
        }
    }

    private void checkForUserIds(int requestUserId, int id) {
        if (requestUserId != id) {
            throw new UnauthorizedOperationException("You must be the logged user.");
        }
    }

    private void validateEmail(String email) {
        if (email.contains("@") && email.contains(".com")) {
            return;
        }
        throw new IllegalArgumentException("Invalid Email Address.");
    }
}
