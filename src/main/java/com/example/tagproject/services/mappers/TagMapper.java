package com.example.tagproject.services.mappers;

import com.example.tagproject.entities.Tag;
import com.example.tagproject.entities.User;
import com.example.tagproject.entities.dto.CreateTagDto;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {

    public Tag dtoToObject(CreateTagDto createTagDto, User user) {
        Tag tag = new Tag();

        tag.setContent(createTagDto.getContent());
        tag.setDescription(createTagDto.getDescription());
        tag.setTitle(createTagDto.getTitle());
        tag.setUser(user);

        user.getTags().add(tag);

        return tag;
    }
}
