package com.example.tagproject.services.mappers;

import com.example.tagproject.entities.User;
import com.example.tagproject.entities.dto.CreateUserDto;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User dtoToObject(CreateUserDto createUserDto) {
        User user = new User();

        user.setEmail(createUserDto.getEmail());
        user.setUsername(createUserDto.getUsername());
        user.setPasswordHash(transformUserPassword(createUserDto.getPassword()));

        return user;
    }

    /**
     * This is a basic password hash just for testing purposes.
     */
    private String transformUserPassword(String password) {
        return "__" + password + "hash3264";
    }
}
