package com.example.tagproject.repositories;

import com.example.tagproject.entities.User;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getUserById(int id);

    User getUserByEmail(String email);

    User getUserByUsername(String username);

    User createUser(User user);

    User updateUser(User user);

    User deleteUser(User user);
}
