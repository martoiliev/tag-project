package com.example.tagproject.repositories;

import com.example.tagproject.entities.Tag;

import java.util.List;

public interface TagRepository {

    List<Tag> getAll();

    Tag getTagById(int id);

    Tag getTagByTitle(String title);

    Tag createTag(Tag tag);

    Tag updateTag(Tag tag);

    Tag deleteTag(Tag tag);
}
