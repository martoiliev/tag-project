package com.example.tagproject.repositories;

import com.example.tagproject.entities.Tag;
import com.example.tagproject.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@PropertySource("classpath:application.properties")
public class TagRepositoryImpl implements TagRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> tags = session.createQuery("from Tag", Tag.class);

            return tags.getResultList();
        }
    }

    @Override
    public Tag getTagById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);

            if (tag == null) {
                throw new EntityNotFoundException(String.format("Tag with id: %d does not exists.", id));
            }

            return tag;
        }
    }

    @Override
    public Tag getTagByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> tag = session.createQuery("from Tag where title = :title", Tag.class);
            tag.setParameter("title", title);

            if (tag.list().size() == 0) {
                throw new EntityNotFoundException(
                        String.format("Tag with title: %s does not exists.", title));
            }

            return tag.list().get(0);
        }
    }

    @Override
    public Tag createTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);

            return tag;
        }
    }

    @Override
    public Tag updateTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();

            return tag;
        }
    }

    @Override
    public Tag deleteTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(tag);
            session.getTransaction().commit();

            return tag;
        }
    }
}
