{\rtf1\ansi\ansicpg1252\cocoartf2639
\cocoatextscaling0\cocoaplatform0{\fonttbl\f0\fmodern\fcharset0 Courier;}
{\colortbl;\red255\green255\blue255;\red191\green100\blue38;\red32\green32\blue32;\red153\green168\blue186;
\red133\green96\blue154;\red86\green132\blue173;}
{\*\expandedcolortbl;;\csgenericrgb\c74902\c39216\c14902;\csgenericrgb\c12549\c12549\c12549;\csgenericrgb\c60000\c65882\c72941;
\csgenericrgb\c52157\c37647\c60392;\csgenericrgb\c33725\c51765\c67843;}
\paperw11900\paperh16840\margl1440\margr1440\vieww11520\viewh8400\viewkind0
\pard\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\pardirnatural\partightenfactor0

\f0\fs26 \cf2 \cb3 create table \cf4 users\
(\
    \cf5 id           \cf2 int auto_increment\
        primary key,\
    \cf5 email        \cf2 varchar\cf4 (\cf6 50\cf4 ) \cf2 null,\
    \cf5 username     \cf2 varchar\cf4 (\cf6 30\cf4 ) \cf2 null,\
    \cf5 passwordHash \cf2 varchar\cf4 (\cf6 30\cf4 ) \cf2 null\
\cf4 )\cf2 ;\
\
create table \cf4 tags\
(\
    \cf5 id          \cf2 bigint auto_increment\
        primary key,\
    \cf5 content     \cf2 longtext     null,\
    \cf5 description \cf2 varchar\cf4 (\cf6 100\cf4 ) \cf2 null,\
    \cf5 title       \cf2 varchar\cf4 (\cf6 50\cf4 )  \cf2 null,\
    \cf5 user_id     \cf2 int          null,\
    constraint \cf4 fk_tags_user\
        \cf2 foreign key \cf4 (\cf5 user_id\cf4 ) \cf2 references \cf4 users (\cf5 id\cf4 )\
)\cf2 ;\
\
\
}